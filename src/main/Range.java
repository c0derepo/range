/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 * Range.java
 *
 * @Author c0der <4c.app.testing@gmail.com>
 * 8 May 2018
 *
 * @param <T>
 */
public class Range<T extends Comparable<T>> {

	/**
	 * Include start, end in {@link Range}
	 */
	public enum Inclusive {

		/**
		 * {@link Range} inclusive of start, exclusive of end
		 */
		START,

		/**
		 * {@link Range} inclusive of end, exclusive of start
		 */
		END,

		/**
		 * {@link Range} inclusive of start and end
		 */
		BOTH,

		/**
		 * {@link Range} exclusive of start and end
		 */
		NONE
	}

	/**
	 * {@link Range} start and end values
	 */
	private T start, end;

	/**
	 *
	 */
	private Inclusive inclusive;

	/**
	 * Auto switch if start > end
	 * <br>It effects {@link #setStart(Comparable)} and {@link #setEnd(Comparable)}
	 * <br>It does not effect constructor
	 * <br>Default is set to false.
	 */
	private boolean isAutoSwitch = false;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Create a range with {@link Inclusive#START}
	 *
	 * @param start
	 *<br/> Not null safe
	 * @param end
	 *<br/> Not null safe
	 *<br/>Auto switched if start > end
	 */
	public Range(T start, T end) {

		this(start, end, null);
	}

	/**
	 * @param start
	 *<br/> Not null safe
	 * @param end
	 *<br/> Not null safe
	 *<br/>Auto switched if start > end
	 *@param inclusive
	 *<br/>If null {@link Inclusive#START} used
	 */
	public Range(T start, T end, Inclusive inclusive) {

		if((start == null) || (end == null)) {

			throw new NullPointerException("Invalid null start / end value");
		}
		setInclusive(inclusive);

		if( isBigger(start, end) ) {

			this.start = end;
			this.end   = start;

		}else {

			this.start = start;
			this.end   = end;
		}

	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Check if this {@link Range} contains t
	 *
	 *@param t
	 *<br/>Not null safe
	 *@return
	 *false for any value of t, if this.start equals this.end
	 */
	public boolean contains(T t) {

		return contains(t, inclusive);
	}

	/**
	 * Check if this {@link Range} contains t
	 *
	 *@param t
	 *<br/>Not null safe
	 *@param inclusive
	 *<br/>If null {@link Range#inclusive} used
	 *@return
	 *false for any value of t, if this.start equals this.end
	 */
	public boolean contains(T t, Inclusive inclusive) {

		if(t == null) {

			throw new NullPointerException("Invalid null value");
		}

		inclusive = (inclusive == null) ? this.inclusive : inclusive;

		switch (inclusive) {

			case NONE:
				return ( isBigger(t, start) && isSmaller(t, end) );

			case BOTH:
				return ( ! isBigger(start, t)  && ! isBigger(t, end) ) ;

			case START: default:
				return ( ! isBigger(start, t)  &&  isBigger(end, t) ) ;

			case END:
				return ( isBigger(t, start)  &&  ! isBigger(t, end) ) ;
		}
	}

	/**
	 * Check if this {@link Range} contains other range
	 *
	 * @return
	 * false for any value of range, if this.start equals this.end
	 */
	public boolean contains(Range<T> range) {

		return contains(range.start) && contains(range.end);
	}

	/**
	 * Check if this {@link Range} intersects with other range
	 *
	 * @return
	 * false for any value of range, if this.start equals this.end
	 */
	public boolean intersects(Range<T> range) {

		return contains(range.start) || contains(range.end);
	}

	/**
	 * Convenience method
	 */
	public static <T extends Comparable<T>> boolean isBigger(T t1, T t2) {

		return t1.compareTo(t2) > 0;
	}

	/**
	 * Convenience method
	 */
	public static <T extends Comparable<T>> boolean isSmaller(T t1, T t2) {

		return t1.compareTo(t2) < 0;
	}

	/**
	 * Modifies range, if needed, so
	 * range.getStart() >= intoOtherRange.getStart() and <br/>
	 * range.getEnd() <= intoOtherRange.getEnd()
	 * It does not guarantee into.contains(range)==true which depends also on
	 * {@link Range#inclusive}
	 * @param range
	 * @param into
	 * <br>Both not null safe
	 */
	public static <T extends Comparable<T>>	Range<T> fit(Range<T> range,
															 Range<T> into) {
		if(isBigger(into.getStart(), range.getStart()) //start too small
				|| isBigger(range.getStart(),into.getEnd())) { //start too big
			range.setStart(into.getStart());
		}

		if(isBigger(into.getStart(), range.getEnd()) //end too small
				|| isBigger(range.getEnd(),into.getEnd())) { //start too big
			range.setEnd(into.getEnd());
		}

		return range;
	}

	/**
	 * Modifies range, if needed, so
	 * range.getStart() <= intoOtherRange.getStart() and <br/>
	 * range.getEnd() >= intoOtherRange.getEnd() <br/>
	 * It does not guarantee range.contains(toContain)==true which depends also on
	 * {@link Range#inclusive}
	 * @param range
	 * @param toContain
	 * <br>Both not null safe
	 */
	public static <T extends Comparable<T>>	Range<T> expand(Range<T> range,
													Range<T> toContain) {
		if(isBigger(range.getStart(), toContain.getStart()) //start too big
				|| isBigger(range.getStart(),toContain.getEnd())) { //start too big
			range.setStart(toContain.getStart());
		}

		if(isBigger(toContain.getStart(), range.getEnd()) //end too small
				|| isBigger(toContain.getEnd(), range.getEnd())) { //end too small
			range.setEnd(toContain.getEnd());
		}

		return range;
	}

	/**
	 * Returns T which is within min (inclusive) , max (inclusive)
	 *
	 *@param value, min, max
	 * <br/>Not null safe
	 * <br/> if min > max they are switched
	 * @return
	 * min if value is smaller than min
	 * <br/>max if value is bigger than max
	 * <br/>value otherwise
	 */
	public static <T extends Comparable<T>>	T setWithin(T value, T min, T max) {
		Range<T> range = new Range<>(min, max, Inclusive.BOTH);
		if( Range.isBigger(value, range.getEnd())){

			value = range.getEnd();

		}else if(Range.isBigger(range.getStart(), value)) {

			value = range.getStart();
		}

		return value;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {

		return "Range<"+ getStart().getClass().getSimpleName()
								+ "> "+ getStart() +"-" +getEnd();
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	* Get {@link #start}
	*/
	public T getStart() { return start; }

	/**
	* Set {@link #start}
	* <br/>Not null safe
	* <br/>If {@link #isAutoSwitch} is set to true, and  start > end
	* they are switched
	*/
	public Range<T> setStart(T start) {

		if(isAutoSwitch && (start.compareTo(end)>0)) {
			this.start = end;
			this.end  = start;
		}else {
			this.start = start;
		}
		return this;
	}

	/**
	* Get {@link #end}
	*/
	public T getEnd() {  return end;  }

	/**
	* Set {@link #end}
	* <br/>Not null safe
	* <br/>If {@link #isAutoSwitch} is set to true, and  start > end
	* they are switched
	*/
	public  Range<T> setEnd(T end) {

		if(isAutoSwitch && (start.compareTo(end)>0)) {
			this.end  = start;
			this.start = end;
		}else {
			this.end = end;
		}
		return this;
	}

	/**
	* Get {@link #inclusive}
	*/
	public Inclusive getInclusive() { return inclusive; }

	/**
	* Set {@link #inclusive}
	* @param inclusive
	*<br/>If null {@link Inclusive#START} used
	*/
	public  Range<T> setInclusive(Inclusive inclusive) {

		this.inclusive = (inclusive == null) ? Inclusive.START : inclusive;
		return this;
	}

	/**
	* Get {@link #isAutoSwitch}
	*/
	public boolean isAutoSwitch() { return isAutoSwitch; }

	/**
	* Set {@link #isAutoSwitch}
	*/
	public Range<T> setAutoSwitch(boolean isAutoSwitch) {

		this.isAutoSwitch = isAutoSwitch;
		return this;
	}
}
